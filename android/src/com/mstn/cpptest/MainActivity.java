package com.mstn.cpptest;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;


public class MainActivity extends Activity {

	static{
		System.loadLibrary("cppTest");
	}
	
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int n1 =8;
        int n2 =6;
        setContentView(R.layout.activity_main);
        String sumaText = String.valueOf(CppWrapper.sumar(n1,n2));
        String restaText = String.valueOf(CppWrapper.restar(n1,n2));
        String multiplicaText = String.valueOf(CppWrapper.multiplicar(n1,n2));
        String divideText = String.valueOf(CppWrapper.dividir(n1,n2));
        String raizText = String.valueOf(CppWrapper.raiz(n1));        
        
        TextView suma = (TextView)findViewById(R.id.suma);
        TextView resta = (TextView)findViewById(R.id.resta);
        TextView multiplica = (TextView)findViewById(R.id.multiplica);
        TextView divide = (TextView)findViewById(R.id.divide);
        TextView raiz = (TextView)findViewById(R.id.raiz);
        
        suma.setText(sumaText);
        resta.setText(restaText);
        multiplica.setText(multiplicaText);
        divide.setText(divideText);
        raiz.setText(raizText);
        
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
