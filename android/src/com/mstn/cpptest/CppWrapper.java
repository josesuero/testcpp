package com.mstn.cpptest;

public class CppWrapper {
	static{
		System.loadLibrary("cppTest");
	}
	public static native int sumar(int n1,int n2);
	public static native int restar(int n1,int n2);
	public static native int multiplicar(int n1,int n2);
	public static native int dividir(int n1,int n2);
	public static native int raiz(int n1);
}
