#include <jni.h>
#include "mstnlib.h"

#ifdef __cplusplus
extern "C"{
#endif

mstnlib lib;

	jint Java_com_mstn_cpptest_CppWrapper_sumar(JNIEnv *env, jobject obj,jint n1,jint n2){
		return lib.sumar(n1,n2);
	}

	jint Java_com_mstn_cpptest_CppWrapper_restar(JNIEnv *env, jobject obj,jint n1,jint n2){
		return lib.restar(n1,n2);
	}
	jint Java_com_mstn_cpptest_CppWrapper_multiplicar(JNIEnv *env, jobject obj,jint n1,jint n2){
		return lib.multiplicar(n1,n2);
	}
	jint Java_com_mstn_cpptest_CppWrapper_dividir(JNIEnv *env, jobject obj,jint n1,jint n2){
		return lib.dividir(n1,n2);
	}

	jint Java_com_mstn_cpptest_CppWrapper_raiz(JNIEnv *env, jobject obj,jint n1){
		return lib.raiz(n1);
	}

#ifdef __cplusplus
}
#endif
