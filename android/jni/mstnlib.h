//
//  mstnlib.h
//  
//
//  Created by Jose Suero on 4/21/15.
//
//

#ifndef ____mstnlib__
#define ____mstnlib__

#include <stdio.h>
#include <math.h>


class mstnlib
{
public:
    int sumar(int n1,int n2);
    int restar(int n1,int n2);
    int multiplicar(int n1,int n2);
    int dividir(int n1,int n2);
    double raiz(int n1);
};



#endif /* defined(____mstnlib__) */
