//
//  mstnlib.cpp
//  
//
//  Created by Jose Suero on 4/21/15.
//
//

#include "mstnlib.h"
#include <string.h>

int mstnlib::sumar(int n1, int n2){
    return n1+n2;
}

int mstnlib::restar(int n1, int n2){
    return n1-n2;
}

int mstnlib::multiplicar(int n1, int n2){
    return n1*n2;
}

int mstnlib::dividir(int n1, int n2){
    return n1/n2;
}

double mstnlib::raiz(int n1){
    return sqrt(n1);
}