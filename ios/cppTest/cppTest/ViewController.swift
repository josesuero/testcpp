//
//  ViewController.swift
//  cppTest
//
//  Created by Jose Suero on 4/21/15.
//  Copyright (c) 2015 Jose Suero. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    @IBOutlet weak var suma: UILabel!
    @IBOutlet weak var resta: UILabel!
    @IBOutlet weak var multiplica: UILabel!
    @IBOutlet weak var divide: UILabel!
    @IBOutlet weak var raizlabel: UILabel!


    override func viewDidLoad() {
        super.viewDidLoad()
        
        let n1:Int32 = 6
        let n2:Int32 = 6
        
        let sumarResult = libSumar(n1,n2)
        let restarResult = libRestar(n1,n2)
        let multiplicarResult = libMultiplicar(n1,n2)
        let dividirResult = libDividir(n1,n2)
        let raizResult = libRaiz(n1)
         
        suma.text = String(sumarResult)
        resta.text = String(restarResult)
        multiplica.text = String(multiplicarResult)
        divide.text = String(dividirResult)
        raizlabel.text = String(format:"%f", raizResult)
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

