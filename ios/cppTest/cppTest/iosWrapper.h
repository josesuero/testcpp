//
//  iosWrapper.h
//  cppTest
//
//  Created by Jose Suero on 4/21/15.
//  Copyright (c) 2015 Jose Suero. All rights reserved.
//


int libSumar(int n1,int n2);
int libRestar(int n1,int n2);
int libMultiplicar(int n1,int n2);
int libDividir(int n1,int n2);
double libRaiz(int n1);

#ifndef cppTest_iosWrapper_h
#define cppTest_iosWrapper_h



#endif
