//
//  iosWrapper.cpp
//  cppTest
//
//  Created by Jose Suero on 4/21/15.
//  Copyright (c) 2015 Jose Suero. All rights reserved.
//

#include "mstnlib.h"

mstnlib lib;
#ifdef __cplusplus
extern "C" {
#endif
    int libSumar(int n1,int n2){
        return lib.sumar(n1,n2);
    }
    int libRestar(int n1,int n2){
        return lib.restar(n1,n2);
        
    }
    int libMultiplicar(int n1,int n2){
        return lib.multiplicar(n1,n2);
        
    }
    int libDividir(int n1,int n2){
        return lib.dividir(n1,n2);
        
    }
    
    double libRaiz(int n1){
        double result = lib.raiz(n1);
        return result;
    }
#ifdef __cplusplus
}
#endif